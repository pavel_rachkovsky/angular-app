angular
	.module('musicApp')
	.controller('artistsController', ['$scope', '$state', 'artists', function ($scope, $state, artists) {
		var vm = this;
		
		artists.all().then(function(artist){
			vm.artist = artist;
		});
	
		vm.remove = remove;

		function remove(index, id) {

			vm.artist.splice(index, 1);
		
			artists.remove(id).then(function(res,err) {
				
				if(!err){
					$state.reload();
									
				}
			}); 
		}
}]);
