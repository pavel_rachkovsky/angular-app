angular
	.module('musicApp')
	.controller('addArtistController', ['$scope', '$state', '$timeout', 'artists', function ($scope, $state, $timeout, artists) {
		
		var vm = this;
		
		vm.add = add;
		vm.allow = null;
		vm.name ='';
		vm.checkName = checkName;
		vm.checkInput = checkInput;

		var timer;

		function add() {

			artists.add({
				name: vm.name,
				genre: vm.genre,
				image: vm.image
				
			}).then(function(res){

					$state.go('artists');

			}).catch(function(err){
				
				console.log(err);

			});

		}

		function checkInput(){
		
			$timeout.cancel(timer);
				
			timer = $timeout(checkName, 1000);

			vm.loading = true

		}


		function checkName(){


			artists.checkName({name: vm.name}).then(function(response){

				vm.allow = response.data.allow;

				vm.loading = false;

				console.log(vm.allow);

				if (!vm.name){
				
					vm.allow = undefined;

				}

			});
		}


}]);
