var mongoose = require('mongoose');

var ArtistSchema = new mongoose.Schema({
	name: {type: String, unique: true}}, 
	{collection: "Artists", strict: false});

module.exports = mongoose.model('Artist', ArtistSchema);