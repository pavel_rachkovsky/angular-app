angular
	.module('musicApp')
	.controller('editAlbumController', ['$scope', '$state', '$stateParams', 'albums', function ($scope, $state, $stateParams, albums) {
		
		var vm = this;

		vm.edit = edit;
		vm.addSong = addSong;
		vm.removeSong = removeSong;

		vm.id = $stateParams.id;

		albums.getById(vm.id).then(function(album){
			vm.album = album;
		});

		function edit(){
			albums.update(vm.id, 
				
			{
				name: vm.album.name,
				artist_name: vm.album.artist_name,
				artist_id: vm.album.artist_id,
				image: vm.album.image,
				songs: vm.album.songs,
			});
		}

		function addSong(){

			vm.album.songs.push({});

		}

		function removeSong(index){

			vm.album.songs.splice(index, 1);

		}

}]);
