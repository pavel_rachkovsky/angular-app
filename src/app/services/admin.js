angular
	.module('musicApp')
	.factory('admin', ['$http', '$cookies','$rootScope', function ($http, $cookies, $rootScope) {
    	var admin = {
    		
            login: login,
            logout: logout,
            checkAuth: checkAuth

    	};

    	return admin;

    	function login(data) {
    		return $http.post('/login', data).then(function(response){
    			return response.data;
    		});
    	}

        function logout(){
            return $http.get('/logout').then(function(response){
                return response.data;
            }); 

        }

        function checkAuth(){
            return $cookies.get('session') ? true : false;
        }

	}]);
