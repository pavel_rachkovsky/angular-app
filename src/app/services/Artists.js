angular
	.module('musicApp')
	.factory('artists', ['$http', function ($http) {
    	var artists = {
    		all: all,
    		add: add,
            remove: remove,
            update: update,
            getById: getById,
            checkName: checkName
    	};

    	return artists;

    	function all() {
    		return $http.get('/artists').then(function(response) {
    			return response.data;
    		});
    	}

        function getById(id){
            return $http.get('/artists/' + id).then(function(response) {
                return response.data;
            });          
        }

    	function add(artist) {
			return $http.post('/artists/new', artist);
    	}

        function checkName(name){
            return $http.post('/artists/check_name', name);  
        }

        function remove(id) {
            return $http.delete('/artists/' + id);
        }

        function update(id, artist) {
            return $http.put('/artists/edit/' + id, artist);    
        }

	}]);
