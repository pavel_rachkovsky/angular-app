angular
	.module('musicApp')
	.factory('albums', ['$http', function ($http) {
    	var albums = {
    		all: all,
    		add: add,
            remove: remove,
            update: update,
            getById: getById
    	};

    	return albums;

    	function all() {
    		return $http.get('/albums').then(function(response) {
    			return response.data;
    		});
    	}

        function getById(id){
            return $http.get('/albums/' + id).then(function(response) {
                return response.data;
            });          
        }

    	function add(album) {
			return $http.post('/albums/new', album);
    	}

        function remove(id) {
            return $http.delete('/albums/' + id);
        }

        function update(id, album) {
            return $http.put('/albums/edit/'+ id, album);
        }

	}]);
