angular
	.module('musicApp')
	.controller('adminController', ['$scope', '$state', 'admin', function ($scope, $state, admin) {
		
		var vm = this;

		vm.login = login;
		vm.logout = logout;
	

		function login(){
			admin.login({
				name: vm.name,
				password: vm.password
			}).then(function(response){
				console.log(response);
			});		
		}

		function logout(){
			admin.logout().then(function(response){
				console.log(response);
			});
		}

}]);
