angular
	.module('musicApp')
	.controller('addAlbumController', ['$document', '$scope', '$state', 'artists', 'albums', function ($document, $scope, $state, artists, albums) {
		
		var vm = this;
		vm.songs = [];


		albums.all().then(function(album){
			vm.album = album;
		});

		artists.all().then(function(artist){
			vm.artist = artist;
		});
		
		vm.addAlbum = addAlbum;
		vm.addSong = addSong;
		vm.remove = remove;


		function addAlbum() {

			albums.add({
				
				name: vm.album.name,
				artist_id: vm.selectedArtist._id,
				artist_name: vm.selectedArtist.name,
				image: vm.album.image,
				songs: vm.songs

				
			}).then(function(res,err){
				
				// $state.go('albums');
				if (err){
					console.log(err)
				}else{
					console.log(res)
				}

			}).catch(function(err){

				console.log(err);

			});

		}


		function addSong(){

			vm.songs.push({});

		}

		function remove(index){

			vm.songs.splice(index, 1);

		}

}]);
