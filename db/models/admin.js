var mongoose = require('mongoose');

var AdminSchema = new mongoose.Schema({

	name: String,
	password: String

}, {collection: "Admin", strict: false});


module.exports = mongoose.model('Admin', AdminSchema);