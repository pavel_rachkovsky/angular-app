var mongoose = require('mongoose');
var crypto = require('crypto');
var Artist = require('./../db/models/artist.js');
var Album = require('./../db/models/album.js');
var async = require('async');



exports.createArtist = function(artistData){

	var newArtist = new Artist(artistData.body);
	return newArtist.save();
}

exports.updateArtist = function(artistData){

	var updatedArtist = Album.findByIdAndUpdate(artistData.params.id, artistData.body, {new: true});
	return updatedArtist;
}

exports.deleteArtist = function (artistData){

	var deletedArtist = Artist.findByIdAndRemove(artistData.params.id);
	return deletedArtist;
}

exports.readArtist = function (req, res){

	var data = {};

	return async.parallel(

	{
		artist: function(callback) {
			Artist.findById(req.params.id, function(err, res) {		
				data.artist = res;
				callback(null, res);			
			});
		},

		relatedAlbums: function(callback) {
			Album.find({artist_id: req.params.id }, function(err, res){
				data.relatedAlbums = res;
				callback(null, res);
			});
		}
	},

	function(err, result){
		if (err){
			res.send(data);
		}else{		
			res.send(result);
		}
	});
}

exports.readAllArtists = function(){

	var readAllArtists = Artist.find();
	return readAllArtists;
}


exports.checkArtistName = function(artistData, res){

	var name = artistData.body.name;

	return Artist.find({name: name}, function(err, matches) {
		if (matches.length !== 0){
			res.send({allow: false});
		}else{
			res.send({allow: true});
		}
	});
}