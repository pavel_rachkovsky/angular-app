var mongoose = require('mongoose');
var crypto = require('crypto');
var Album = require('./../db/models/album.js');
// mongoose.connect('mongodb://root:root@ds019836.mlab.com:19836/music_app');


exports.createAlbum = function(albumData){

	var newAlbum = new Album(albumData.body);
	return newAlbum.save();

}

exports.updateAlbum = function(albumData){

	var updatedAlbum = Album.findByIdAndUpdate(albumData.params.id, albumData.body, {new: true});
	return updatedAlbum;

}

exports.deleteAlbum = function (albumData){

	var deletedAlbum = Album.findByIdAndRemove(albumData.params.id);
	return deletedAlbum;

}

exports.readAlbum = function (albumData){

  	return Album.findById(albumData.params.id);
  	
}

exports.readAllAlbums = function(){

	var readAllAlbums = Album.find();
	return readAllAlbums;

}