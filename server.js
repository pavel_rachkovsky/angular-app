var express = require('express');
var app = express();
var mongoose = require('mongoose');

var bodyParser = require('body-parser');
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);

// Database models
var Admin  = require('./db/models/admin.js');
var Album  = require('./db/models/album.js');
var Artist = require('./db/models/artist.js');


// Api for db models
var album_api  = require('./api/album.js');
var artist_api = require('./api/artist.js');


mongoose.connect('mongodb://root:root@ds019836.mlab.com:19836/music_app');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.on('connected', function () {  
	console.log('Mongoose connected');
});     

app.set('port', (process.env.PORT || 5000));

app.use(express.static('dist'));
app.use(favicon(__dirname + '/dist/images/favicon.ico'));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(session({
		secret: '5bfkjvkjYNKU2789GFklv',
		key: 'session',
		resave: false,
		saveUninitialized: false,
	  	store: new MongoStore({ 
    		url: 'mongodb://root:root@ds019836.mlab.com:19836/music_app',
    		clear_interval: 1
  		}),
		cookie: {maxAge: 60 * 1000, httpOnly: false}
	}
));


app.get('/albums', function(req, res){

	album_api.readAllAlbums(req)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		console.log(err);
	})
});

app.get('/albums/:id', function(req, res){

	album_api.readAlbum(req)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		console.log(err);
	})
});

app.delete('/albums/:id', function(req, res){

	album_api.deleteAlbum(req)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		console.log(err);
	})
});

app.post('/albums/new', function(req, res, next){
	
	album_api.createAlbum(req)
	.catch(function(err){
		console.log(err);
	})
});

app.put('/albums/edit/:id', function(req, res, next){

	album_api.updateAlbum(req)
	.catch(function (err){
		console.log(err);
	})
});

app.get('/artists', function(req, res){

	artist_api.readAllArtists(req)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		console.log(err);
	})
});


app.get('/artists/:id', function(req, res){

	artist_api.readArtist(req, res);
});

app.post('/artists/new', function(req, res, next){
	
	artist_api.createArtist(req)
	.catch(function(err){
		console.log(err);
	})
});

app.delete('/artist/:id', function(req, res){

	artist_api.deleteArtist(req)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		console.log(err);
	})
});

app.put('/artists/edit/:id', function(req, res, next){

	artist_api.updateArtist(req)
	.catch(function (err){
		console.log(err);
	})
});


app.post('/artists/check_name', function(req, res){
  	
	artist_api.checkArtistName(req, res);
});


app.post('/login', function(req, res){

	Admin.findOne({name: req.body.name, password: req.body.password}, function(err, admin){

		if (admin){
			req.session.admin = {id: admin._id, name: admin.name}
			res.send("Access allowed");
		}else{		
			res.send("Wrong username or password");
		}
	});
});

app.get('/logout', function(req, res){

	if (req.session.admin){
		delete req.session.admin;
	}
});

app.get('/*', function(req, res){

    res.sendfile("index.html", { root: __dirname + '/dist' });

});


app.listen(app.get('port'), function() {

  	console.log('Node app is running on port', app.get('port'));

});