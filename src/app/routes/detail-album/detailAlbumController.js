angular
	.module('musicApp')
	.controller('detailAlbumController', ['$scope', '$state', '$stateParams', 'artists', 'albums', function ($scope, $state, $stateParams, artists, albums) {
		
		var vm = this;

		vm.id = $stateParams.id;


		albums.getById(vm.id).then(function(album){
			

			vm.album = album;


		}).then(function(){

	 		artists.getById(vm.album.artist_id).then(function(artist){

				vm.genre = artist.artist.genre;

			});


		});


}]);
