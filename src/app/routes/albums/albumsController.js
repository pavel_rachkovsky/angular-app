angular
	.module('musicApp')
	.controller('albumsController', ['$scope', '$state', 'artists', 'albums', function ($scope, $state, artists, albums) {
		var vm = this;
		
		albums.all().then(function(album){
			vm.album = album;
		});

		artists.all().then(function(artist){
			vm.artist = artist;
		});

		vm.remove = remove;

		function remove(index, id) {

			vm.album.splice(index, 1);
		
			albums.remove(id).then(function(res,err) {
					
				console.log(res);
									
			}).catch(function(err){

				console.log(err);

			});
		}
}]);
