angular
	.module('musicApp')
	.controller('detailArtistController', ['$scope', '$state', '$stateParams', 'artists', 'albums', function ($scope, $state, $stateParams, artists, albums) {
		
		var vm = this;

		vm.id = $stateParams.id;

		artists.getById(vm.id).then(function(data){

			vm.artist = data.artist;
			vm.relatedAlbums = data.relatedAlbums;
		
		});
	

}]);
