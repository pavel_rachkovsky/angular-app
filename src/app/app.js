angular.module('musicApp', ['ui.router', 'ngCookies'])
		.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider) {


		$urlRouterProvider.otherwise('/');
		  
  		  $stateProvider.state('home', {
		  	url: '/',
		  	template: '<h1>Home page</h1>'
		  });

		  $stateProvider.state('albums', {
		  	url: '/albums',
		  	templateUrl: 'routes/albums/albums.html',
		  });

		  $stateProvider.state('new_album', {
		  	url: '/albums/new',
		  	templateUrl: 'routes/add-album/add-album.html',
		  });

		  $stateProvider.state('edit_album', {
		  	url: '/albums/edit/:id',
		  	templateUrl: 'routes/edit-album/edit-album.html',
		  });

		  $stateProvider.state('album', {
		  	url: '/albums/:id',
		  	templateUrl: 'routes/detail-album/detail-album.html'                
		  });

		  $stateProvider.state('artists', {
		  	url: '/artists',
		  	templateUrl: 'routes/artists/artists.html'
		  });

		  $stateProvider.state('new_artist', {
		  	url: '/artists/new',
		  	templateUrl: 'routes/add-artist/add-artist.html'
		  });

		  $stateProvider.state('edit_artist', {
		  	url: '/artists/edit/:id',
		  	templateUrl: 'routes/edit-artist/edit-artist.html'
		  });

		  $stateProvider.state('artist', {
		  	url: '/artists/:id',
		  	templateUrl: 'routes/detail-artist/detail-artist.html'                
		  });

		  $stateProvider.state('admin', {
		  	url: '/admin',
		  	templateUrl: 'routes/admin/admin.html'
		  });

	}]).run()

