var gulp = require('gulp');
var plumber = require('gulp-plumber');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var sassGlob = require('gulp-sass-glob');
var autoprefixer = require('gulp-autoprefixer');
var cssnano = require('gulp-cssnano');
var htmlminify = require('html-minify');
var templateCache = require('gulp-angular-templatecache');
// var webserver = require('gulp-webserver');


gulp.task('default', ['js', 'vendor:js', 'css', 'vendor:css', 'vendor:icons', 'html', 'images', 'fonts', 'templates']);
gulp.task('dev', ['default', 'watch']);


// gulp.task('webserver', function() {
//   gulp.src('dist')
//     .pipe(webserver({
//       livereload: true,
//       fallback: 'index.html'
//     }));
// });


gulp.task('js', function() {
	return gulp.src(['src/app/**/*.js'])
		.pipe(plumber())
		.pipe(concat('app.js'))
		//.pipe(uglify())
		.pipe(plumber.stop())
		.pipe(gulp.dest('dist/js'))

});

gulp.task('vendor:js', function() {
	return gulp.src([
		'bower_components/angular/angular.js',
		'bower_components/angular-ui-router/release/angular-ui-router.js',
		'bower_components/angular-cookies/angular-cookies.js'
	]).pipe(plumber())
		.pipe(concat('vendor.js'))
		.pipe(uglify({mangle: false}))
		.pipe(plumber.stop())
		.pipe(gulp.dest('dist/js'));
});

gulp.task('css', function() {
	gulp.src(['src/styles/init.sass'])
		.pipe(plumber())
		.pipe(sassGlob())
		.pipe(sass())
		.pipe(autoprefixer())
		.pipe(concat('app.css'))
		.pipe(cssnano({zindex: false}))
		.pipe(plumber.stop())
		.pipe(gulp.dest('dist/css'))

});

gulp.task('vendor:css', function() {
	return gulp.src([
		'bower_components/salesforce-lightning-design-system/assets/styles/salesforce-lightning-design-system.css',
	]).pipe(plumber())
		.pipe(concat('vendor.css'))
		.pipe(plumber.stop())
		.pipe(gulp.dest('dist/css'));
});

gulp.task('vendor:icons', function() {
	return gulp.src([
		'bower_components/salesforce-lightning-design-system/assets/icons/**/*',
	]).pipe(gulp.dest('dist/assets/icons'));
});


gulp.task('html', function() {
	gulp.src('src/*.html')
		.pipe(gulp.dest('dist'))
});

gulp.task('templates', function () {
  return gulp.src(['src/app/**/*.html',
  					'!src/app/*.html'])
	  // .pipe(htmlminify())
	.pipe(templateCache('templates.js', {
   		module: 'musicApp'
  }))
    .pipe(gulp.dest('dist/js'))
});

gulp.task('images', function() {
	gulp.src('src/images/**/*')
		.pipe(gulp.dest('dist/images'))
});

gulp.task('fonts', function() {
	gulp.src('bower_components/salesforce-lightning-design-system/assets/fonts/webfonts/*')
		.pipe(gulp.dest('dist/fonts/webfonts'))
});

gulp.task('watch', function() {
	var paths = {
		
		html: [
			'src/*.html'
		],
		js: [
			'src/app/**/*.js'
		],
		styles:[
			'src/styles/**/*'
		],
		images: [
			'src/images/**/*'
		],
		fonts: [
			'src/fonts/**/*'
		],
		templates: [
			'src/app/**/*.html'
		]
	};

	gulp.watch(paths.html, ['html'])
	gulp.watch(paths.js, ['js']);
	gulp.watch(paths.styles, ['css']);
	gulp.watch(paths.images, ['images']);
	gulp.watch(paths.fonts, ['fonts']);
	gulp.watch(paths.templates, ['templates']);


});