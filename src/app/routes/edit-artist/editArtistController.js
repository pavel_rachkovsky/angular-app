angular
	.module('musicApp')
	.controller('editArtistController', ['$scope', '$state', '$stateParams', 'artists', function ($scope, $state, $stateParams, artists) {
		
		var vm = this;

		vm.edit = edit;

		vm.id = $stateParams.id;

		artists.getById(vm.id).then(function(artist){
			vm.artist = artist.artist;
		});

		function edit(){
			artists.update(vm.id, 
			{
				name: vm.artist.name,
				genre: vm.artist.genre,
				image: vm.artist.image
			})
		}

}]);
